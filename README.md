# Developer Notes

Repositório para relatar coisas do desenvolvimento durante a disciplina GCES 2022.1

## Sprints

[Sprint 01](docs/sprint01/README.md)

[Sprint 02](docs/sprint02/README.md)
